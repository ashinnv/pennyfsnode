package nodes

import (
	"os"

	"gitlab.com/ashinnv/blocks"

	"encoding/gob"
	"net"
	"time"
)

type Node struct {
	blockList []blocks.Block
}

type reqStr struct {
	BlockName string
	UserReqst string
	UserAuths string
}

//Maintain a connection to the master node to listen and send a block when wanted and get a block when received
//storePtr is a pointer to a list of file names being maintained by MaintainFile()
func MaintainConnection(newBlockChan chan blocks.Block, storePtr *[]string) {

	var newBlk blocks.Block
	var requestData reqStr

	//Create a connection for getting new blocks
	manRecLn, lnErr := net.Listen("tcp", ":9045")
	if lnErr != nil {
		time.Sleep(3 * time.Second)
		manRecLn.Close()
		go MaintainConnection(newBlockChan, storePtr)
		return
	}

	go func() {
		for {
			manCon, accErr := manRecLn.Accept()
			if accErr != nil {
				time.Sleep(3 * time.Second)
				manCon.Close()
				go MaintainConnection(newBlockChan, storePtr)
				return
			}

			dec := gob.NewDecoder(manCon)
			dec.Decode(&newBlk)

		}
	}()

	//Create a connection to receive requests for blocks and return said block
	manSndLn, MSLErr := net.Listen("tcp", ":9046")
	if MSLErr != nil {
		time.Sleep(3 * time.Second)
		manSndLn.Close()
		go MaintainConnection(newBlockChan, storePtr)
		return
	}

	go func() {
		for {
			sndConn, sConErr := manSndLn.Accept()
			if sConErr != nil {
				time.Sleep(3 * time.Second)
				sndConn.Close()
				go MaintainConnection(newBlockChan, storePtr)
				return
			}

			dec := gob.NewDecoder(sndConn)
			dec.Decode(&requestData)

		}
	}()
}

//Maintain a directory that holds the blocks that exist on this node as a database.
func MaintainFiles(blockRequestLine chan string, blockReturnLine chan blocks.Block) {
	associationSlice := make(map[string]os.File) //Map containing block hashes and associated file handles
	cacheSlice := make(map[string]blocks.Block)  //Cached blocks for this node

}
